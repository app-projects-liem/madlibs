#!/usr/bin/env python
from .Estado import Estado
class MaquinaEstado:

    def __init__(self):

        self.estadoActual = 0
        self.colEstados = list()

    def agregarEstado(self, estado):
        self.colEstados.append(estado)
    
    def obtenerEstados(self) :
        return self.colEstados
    
    def cambiarEstado(self, numero) : 
        self.estadoActual = numero
    
    def iniciarMaquinaEstado(self) :
        opcion = self.colEstados[self.estadoActual].ejecutar()
        self.estadoActual = opcion

        
