from .Estado import Estado


class MenuPrincipal (Estado):
    def __init__(self, nombre):
        Estado.__init__(self, nombre)

    def ejecutar(self):
        opcion = 0
        while(opcion == 0):
            Estado.limpiarConsola(self)

            cadenaJuego1 = "░░█ █░█ █▀▀ █▀▀ █▀█\n"
            cadenaJuego2 = "█▄█ █▄█ ██▄ █▄█ █▄█\n"
            print(cadenaJuego1 + cadenaJuego2)

            madlibs1 = "███╗░░░███╗░█████╗░██████╗░██╗░░░░░██╗██████╗░░██████╗\n"
            madlibs2 = "████╗░████║██╔══██╗██╔══██╗██║░░░░░██║██╔══██╗██╔════╝\n"
            madlibs3 = "██╔████╔██║███████║██║░░██║██║░░░░░██║██████╦╝╚█████╗░\n"
            madlibs4 = "██║╚██╔╝██║██╔══██║██║░░██║██║░░░░░██║██╔══██╗░╚═══██╗\n"
            madlibs5 = "██║░╚═╝░██║██║░░██║██████╔╝███████╗██║██████╦╝██████╔╝\n"
            madlibs6 = "╚═╝░░░░░╚═╝╚═╝░░╚═╝╚═════╝░╚══════╝╚═╝╚═════╝░╚═════╝░\n"

            print(madlibs1 + madlibs2 + madlibs3 + madlibs4 + madlibs5 + madlibs6)
            print("---------------------------------------------------------------------")
            print("1. Empezar juego.")
            print("2. Modificar reglas.")
            print("3. Salir.")
            print("---------------------------------------------------------------------")
            opcion = int(input("Selecciona pe: "))
        return opcion
