### Id: #6

### Función: Verificar si sigue ronda

### Descripción: 

La función comprueba si la oración solicitada es correcta. 

Si el valor boleano recibido como argumento es verdadero, termina 
la ronda y se suma un punto al ganador( de la ronda), además de agregar la oración ganadora.
En otro caso se continua con el siguiente turno.

Esta función es utilizada cuando un jugador solicita verificar su oración.

### UseCases internos
### "Pasos" 

var esGanador = VerificaGanadorOracion()
VerificaGanadorRonda(esGanador)
### UseCases relacionados (externos)
- jugarRonda()
- VerificaGanadorOracion()

### Precondición
### Poscondición

- Puede determinar si hay ganador de ronda o si continúa la misma.
### Garantía de fallo

### Prioridad: ALTA

### Requirimientos no funcionales - Requisitos especiales

- Muestra un cartel de que esta evualuando si sigue ronda o hay ganador
- Simular un tiempo para calcular el recuento de votos.


