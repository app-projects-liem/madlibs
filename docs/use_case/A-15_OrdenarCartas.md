### Id: 15 

### Función: Ordenar mis cartas

### Descripción:

Esta función se encarga de administrar u ordenar cartas a su disposición. 
Hasta que el jugador ordene sus cartas la función no finalizará. El jugador
indica cuando salir. 

(opcional) - Quizás se deba agregar un tiempo por jugador.

### Precondición

- Sentencia

### Relaciones con otros UseCases

- intercambiar() - Sentencia
- mostrarSentencia() - Sentencia
- buscarCarta(nroCarta) : boolean
- quitarCarta(nroCarta) : boolean
- ponerCarta(pos, nroCarta)
- buscarPosicionCarta(nroCarta) : int


### Poscondición

- sentencia modificada  
- muestra cartas ordenadas  

### Garantía de fallo

- Error de opción 
- No existe la carta
- Sentencia.

### Prioridad ALTA

### Requirimientos no funcionales - Requisitos especiales