### Id: 14

### Función: Dejar carta mazo descarte - DeckManager

### Descripción: 

Una vez que el jugador eligió una carta del mazo de descarte o una nueva carta,
el jugador debe descartar una carta que no le sea de utilidad, la función 
se centra en agregar una carta descartada por el jugador y agregarla al mazo
para que otros jugadores puedan tomarla o simplemente ignorarla.

### Precondición

- Carta

### Poscondición

- Se agregó una carta nueva al mazo de descarte

### Relaciones con otros UseCases



### Garantía de fallo

### Prioridad (baja - intermedia - alta)

### Requirimientos no funcionales - Requisitos especiales