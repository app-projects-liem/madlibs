### Id: 20

### Función: Agregar Voto a la encuesta 

### Descripción

Recibe un valor como un argumento de tipo Voto y se 
agrega al conjunto de votos que tiene la encuesta.

### Precondición

- Encuesta
- Voto no sea vacio

### Poscondición

- Encuesta con un voto más

### Relaciones con otros UseCases

- EnviarSolicitudJugador()

### Garantía de fallo

- Que el voto no sea nulo
- Que exista la encuesta

### Prioridad Media

### Requirimientos no funcionales - Requisitos especiales