### Id: #5

### Función: Verificar ganador de la oracion 

### Descripción: 

La función depende de los jugadores, por lo siguiente, se debe realizar una 
encuesta sobre la oración de si es correcta o no. Enviar la encuesta a cada 
jugador si la oración es correcta. La misma, es por *si* o por *no*.  

De esto se concluye:

Dependiendo el resultado de la encuesta, osea mayor votos a favor, la ronda se 
da por ganada y devuelve verdadero, en otro caso, devuelve falso y la 
ronda sigue. 

Recibe como argumento una lista de jugadores, jugador interesado y 
la oración a verificar.
Se generará una encuesta a partir de la cantidad de jugadores. 
Se realiza de forma secuencial.

|Encuesta| 
        -> oracion 
        -> voto   

jugador que presenta la oración para evaluar

----

### UseCases internos

- enviarEncuesta(jugador, encuesta) : encuesta
- sigueElJuego(encuesta) : boolean
- generarEncuesta(oracion) : encuesta


/*
encuesta -> votosAFavor -> 
            votosEnContra -> 
            oracion -> 
            
            votar(eleccion) -> mostrar mensaje es correcta o no.
            esAceptada() : boolean 
*/ 

### Precondición

- Que existan jugadores (la lista no sea nula).
- Jugador interesado.
- La oración no sea nulo.
### Poscondición

- devuelve un booleano correspondiente si la ronda continua o no

### Garantía de fallo

- la lista jugadores.
- la oración.
- jugador interesado.

### Prioridad: ALTA

### Requirimientos no funcionales - Requisitos especiales

-  |---------------------------| 
   | VOTACION                  |
   |---------------------------|
   | Oracion formada           |
   | ¿Cuál es su voto? si / no | 
   
   >> 

Ya voto
  |-------------------| 
  | Recuento           |
  -----------------   |
  | Jugador1 - su voto |
  | Jugador1 - su voto |
  |A favor:  - en contra |

