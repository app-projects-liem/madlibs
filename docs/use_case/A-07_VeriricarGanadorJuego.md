### Id: #7

### Función: Verificar ganador de la Juego 

### Descripción: 

La función verifica quien llego al tope de rondas solicitadas para ganar
el juego. 
Recorre al conjunto de jugadores y solicita cuanto puntos tiene, los puntos
son rondas ganadas. El ganador es el que mayor puntaje tiene.

Esta función se usa cuando la ronda finaliza.

### UseCases internos

### Relaciones con otros UseCases

- mostrarGanador()
### Precondición

### Poscondición

- Devuelve un valor boleano.
### Garantía de fallo

- la lista jugadores.

### Prioridad: ALTA

### Requirimientos no funcionales - Requisitos especiales
