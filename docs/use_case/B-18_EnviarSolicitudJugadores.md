### Id 18

### Función: Enviar solicitud de evaluacion(Encueta) a cada jugador

### Descripción:

  Se encargaria de tomar la solicitud creada, y enviarla a cada jugador.

### Precondición:

- Coleccion de jugadores
- Encuesta

### Poscondición

Se le agrego a cada jugador una encuesta nueva

### Garantía de fallo

No

### Prioridad: Intermedia

### Requirimientos no funcionales - Requisitos especiales