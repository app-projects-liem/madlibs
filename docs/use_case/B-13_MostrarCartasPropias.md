### Id: 13

### Función: Mostrar Cartas propias

### Descripción: 

La función muestra las cartas que tiene el jugador actualmente. Se necesita 
recuperar su sentencia, y que la sentencia sea el encargado de generar como se
mostrara las cartas.

### Precondición

- Jugador existe.

### Poscondición

- Retorna algo de tipo gráfico
- 
### Garantía de fallo

- QUe no exista el jugador.

### Prioridad MEDIA

### Requirimientos no funcionales - Requisitos especiales

Recupera algo de tipo gráfico de sentencia

 ------    ------    ------   ------
|      |  |      |  |      | |      |
|      |  |      |  |      | |      |
 ------    ------    ------   ------
 ------    ------    ------   ------
|      |  |      |  |      | |      |
|      |  |      |  |      | |      |
 ------    ------    ------   ------
