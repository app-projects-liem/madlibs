### Id: 23

### Función: Empezar turno

### Descripción:

Esta función se encarga de mostrarle al jugador actual su menu de 
opciones, para manejar sus cartas y otras cosas que se necesite.
Utiliza un gestor de estados(menus), además cada jugador tiene un manejador
de sentencias(manejador de palabras) que le permite usar funciones 
como ordenar cartas, tomar cartas de descarte, ect.

Función que pide una carta de los mazos, descarte y principal.

El mazo principal las cartas no son mostradas. El mazo descarte van todas
las cartas descartadass por los jugadores, y son visibles.

### Precondición

- jugadorActual
- ManagerMazo ...
- gestorEstados -> si fuera necesario

### Poscondición

- S / A

### Relaciones con otros UseCases

- jugarRonda()
- menuJugador()
- ordenarCartas()
- tomarCarta()
- jugarJugador()

### Garantía de fallo

- Que no queden cartas en el mazo principal.
- Que exista el jugador.
- Error al elegir la carta.

### Prioridad ALTA

### Requirimientos no funcionales - Requisitos especiales

----------------------------------------------------------------------------
* Menu de Nombre jugador

Por favor, Indicar nombre jugador1, etc

-----------------------------
Menu jugadorN
Mostrar carta Descarte.            Mostrar mazo principal

1. Tomar carta mazo principal.
2. Tomar carta mazo descarte.
3. Abandonar

------------------------------

Independientemente de la carta elegida

------------------------------------------
Menu jugadorN - Selección carta - otros

1. Cambiar carta
2. Ordenar mi mazo.
3. Solicitar verificación.
4. Pasar turno
5. Abandonar juego.
Observación: Cuando el jugador sale de este menu
la carta se envia al mazo de descarte.
función EnviarCartaDescarte()

-------------------------------