### Id: #8

### Función: Mostrar ganador

### Descripción: 

La función muestra la infomación del jugador ganador. 
    - Nombre jugador
    - Cantidad de ronda diputadas
    - Oraciones ganadores

Esta función recibe como argumento el jugadorGanador.

### UseCases internos

### Relaciones con otros UseCases

- VerificarGanadorJuego()
  
### Precondición

- Halla un ganador

### Poscondición

- Devuelve información sobre el ganador.

### Garantía de fallo

- La lista jugadores.
  
### Prioridad: ALTA

### Requirimientos no funcionales - Requisitos especiales

-------------------------
|  GANADOR: CHORIPAN    |
|                       |
| Rondas: N             |
|                       |
| Oraciones:            |
|                       |
| ORACION 1:            |
| ORACION ...:          |
| ORACION N:            |
-------------------------

