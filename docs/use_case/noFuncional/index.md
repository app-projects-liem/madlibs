Parte no-funcional de la aplicación.

[Menu general](noFuncional/1a-Menu-Inicio.md)  
[Reglas](noFuncional/1b-Reglas.md)  
[Menu jugador](noFuncional/1c-MenuJugador.md)  
[Selección de carta](noFuncional/1d-SeleccionCarta.md)  
[Menu ordenar carta](noFuncional/1e-MenuOrdenarCartas.md)  
