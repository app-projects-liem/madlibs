
Elegir reglas de juego.

------------------------------------------------------------------------------------
Seleccione la regla que desea modificar

1. Numero rondas para ganar.
2. Numero jugadores.
3. Cantidad cartas para armar oraciones (opcional) (por defecto diez cartas)
0. Salir
------------------------------------------------------------------------------------
    Numero rondas para ganar.
        Indique las rondas a ganar en numeros, presione 0 para salir. 
        POR DEFECTO 5 rondas

Las rondas se han modificado - No se modifico las rondas, queda por defecto (5).

    Numero jugadores.
        Indique la cantidad de jugadores, presione 0 para salir.
        Por defecto: 2 jugadores o 1 jugador

    Cantidad cartas para armar oraciones (opcional) 
    (por defecto diez cartas).
