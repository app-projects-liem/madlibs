### Id #3

### Funcion: Detectar si un mazo es vacio

### Descripción: 

Recibe como argumento un Objeto de tipo Mazo, retorna un boleano. 
La función verifica si la colección del Mazo es vacia, en este caso, 
colección de Cartas.

### Precondicion

El mazo no debe ser nulo. Sea el caso, lanzar excepción.

### Poscondicion: S/A


### Garantia de fallo

El mazo sea nulo.

### Prioridad: Intermedia

### Requirimientos no funcionales - Requisitos especiales