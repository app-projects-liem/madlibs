### Id: 24

### Función: Terminar turno

### Descripción:

La función se encarga de verificar distintas funcionalidades del jugador, entre
ellas;  
Verifica si el jugador ha solicitado revisión de su oración.
Verifica si abandono algún jugador, entre otros.
Verifica si existe algún ganador.  
Por último,independientemente de las funciones anteriores recupera la 
carta extra del jugador(la que sobra).  


### Precondición

- Jugador actual

### Poscondición

- Finaliza turno o ronda, dependiendo el caso.

### Relaciones con otros UseCases

- solicitudEvaluacionOracion(jugadorActual) 
- recuperarCartaJugador(jugadorActual) 
- verificaGanadorOracion()

### Garantía de fallo

- Jugador no tengo carta extra
- No exista el jugador (abandonado)

### Prioridad Alta

### Requirimientos no funcionales - Requisitos especiales