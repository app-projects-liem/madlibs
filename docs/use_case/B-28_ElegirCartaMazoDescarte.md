### Id: 28

### Función: Elegir Carta Mazo

### Descripción:

La función se encarga de tomar una carta del mazo de descarte y 
agregarsela al jugador que solicitó la carta y guardarla en un espacio 
que tiene el jugador (carta_extra). 

### Precondición

- jugadorActual
- mazoPrincipal

### Poscondición

- Se agregó carta extra al jugador

### Relaciones con otros UseCases

- empezarTurno

### Garantía de fallo

- jugadorActual.
- mazoPrincipal es vacio.

### Prioridad MEDIA

### Requirimientos no funcionales - Requisitos especiales