### Id 30

### Función: Intercambiar entre sentencias 

### Descripción

  Se encarga de mover una carta entre sentencias. Cada jugador tiene dos
sentencias. Una sentecia es pensado como un espacio para que el jugador coloque
sus cartas. Dejando un espacio para las cartas ordenadas y otro para las
desordenadas.


#### Ejemplo

  La posicion de las cartas en los mazos se conserva. Es decir, que si la carta
A ocupaba la pocicion 2 y es intercambiada por la carta B, que ocupa la pocision
5, al finalizar quedarian asi:

```

 Carta A pocicion 5 de la sentencia ordenada
 Carta B pocision 2 de la sentencia desordenada.
 
```

### Precondición

- Sentencia origen
- Sentecia destino
- Carta de la sentencia origen
- Carta de la sentencia destino



### Poscondición

  Actualizada sentencia ordenada.
  Actualizada sentencia desordenada.


### Relaciones con otros UseCases

S/A

### Garantía de fallo

- Sacar una carta de una sentencia vacia.


### Prioridad: Alta


### Requirimientos no funcionales - Requisitos especiales

-------------------------------------------------------------
|                                                           |
|   Moviendo la carta al espacio ordenado/desordenado       |
|                                                           |
-------------------------------------------------------------
