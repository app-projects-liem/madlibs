### Id: #10

### Función: Mostrar puntaje

### Descripción: 

La función recupera información de los jugadores, como rondas ganadas
por jugador y devolver la información.

### UseCases internos

- S/A

### Relaciones con otros UseCases

- jugarRonda()
  
### Precondición

- Jugador

### Poscondición

- Devuelve un objeto de información (Puntaje).

### Garantía de fallo

- El jugador exista
  
### Prioridad: media

### Requirimientos no funcionales - Requisitos especiales

Como devuelve la información.

{
    puntaje: {
        nombre: 'gato',
        cantidad_rondas_ganadas: 0,
    },
    {
        nombre: 'gato',
        cantidad_rondas_ganadas: 0,
    }
    .
    .
    .
    .
}