### Id 32

### Función: Recuperar carta extra del jugador

### Descripción

  Se encarga de sacar una carta del jugador cuando finaliza su turno
  y se la agrega al mazo de descarte. Además la carta cambiará su 
  forma de verse de oculta a visible y será la primera carta del mazo
  de descarte. La carta anterior pasa a ser oculta.



### Precondición

- Finalice turno



### Poscondición

- Devuelve una carta extra


### Relaciones con otros UseCases

- terminarTurno()

### Garantía de fallo

- null


### Prioridad: Alta


### Requirimientos no funcionales - Requisitos especiales
--