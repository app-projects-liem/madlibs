# UseCase - Barajar cartas

### Id #1

### Función: Barajar cartas

### Descripcion:

Recibe un mazo de cartas, las ordena de forma aleatoria(Mezclarlo) y devuelve
el mazo de cartas. (Puede que no sea necesario, ya que es un instancia)

### Precondicion

1. El mazo tenga cartas. 

### Poscondicion

1. Devuelve un mazo mezclado de forma aleatoria.

### Garantia de fallo

- El mazo esta vacio

### Requirimientos Especiales

- Texto - Simbolos indicando que se baraja.
  
### Prioridad 
Intermedia