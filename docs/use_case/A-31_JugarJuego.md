### Id 31

### Función: Jugar juego

### Descripción

Función principal encargada de aplicar todos los parametros del juego.
Cantidad de jugadores, cantidad de rondas, difucultdad, etc.
Crea a los jugadores con nombre
Crea un Manejador de mazo, utiliza funciones como:
- GenerarMazo(parametros) //cantidad cartas por mazo.

Crea un ciclo dependiendo de los parametros de juego indicando cuando
se gana un juego, es decir, cuantas rondas son para ganar el juego.

Por cada ciclo, se crea una ronda nueva, y envia a los jugadores actuales, 
manejadorMazo. 

Cada vez que finalice una ronda, se le solicita a los jugadores sus cartas.
Agregandose al maso principal. Además de agregar el mazo de descarte.

Por cada ronda nueva, se mezclan las cartas.//Constructor Ronda.

Para determinar un ganador se verifica que, si alguno de los jugadores
tiene la misma cantidad de rondas ganadas que las requeridas como 
parámetros del juego.


### Precondición

- Parametros del juego

### Poscondición

- Mostrar un ganador si es que hay. 


### Relaciones con otros UseCases

- ...()

### Garantía de fallo

- null


### Prioridad: Alta


### Requirimientos no funcionales - Requisitos especiales
--